function fizzbuzz(maxValue) {
    var output = '';
    for (let i = 1; i <= maxValue; i++) {
        if (i % 2 === 0 && i % 3 === 0) {
            output += 'Fizzbuzz,';
        } else if (i % 2 === 0) {
            output += 'Fizz,';
        } else if (i % 3 === 0) {
            output += 'Buzz,';
        } else {
            output += i + ',';
        }
    }
    return output;
}
document.write(fizzbuzz(1000))